using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace server
{
    class Program
    {
        private static void ProcessClientRequests(object argument)
        {
            TcpClient client = (TcpClient)argument;
            try
            {

                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());

                int operation = 0;
                double result = 0;

                while (true)
                {

                    string a1 = reader.ReadLine();
                    double FrstNum = Convert.ToDouble(a1);
                    Console.WriteLine("Angka Pertama: {0}", a1);

                    string o = reader.ReadLine();
                    Console.WriteLine("Operator: {0}", o);

                    string a2 = reader.ReadLine();
                    double ScndNum = Convert.ToDouble(a2);
                    Console.WriteLine("Angka Kedua: {0}", a2);

                    if (o == "+")
                    {
                      
                        
                        result = FrstNum + ScndNum;
                       
                    }
                    else if (o == "-")
                    {
                        result = FrstNum - ScndNum;

                    }
                    else if (o == "*")
                    {
                        result = FrstNum * ScndNum;
                    }
                    else if (o == "/")
                    {
                        result = FrstNum / ScndNum;
                    }
                    

                    Console.WriteLine("\nHasil dari " + FrstNum + " " + o + " " + ScndNum + " = " + result + ".");
                    string hasil = Convert.ToString(result);
                    writer.WriteLine("Hasil : " + hasil);
                    writer.Flush();
                    Console.WriteLine();
                    
                }
                reader.Close();
                writer.Close();
                client.Close();
                Console.WriteLine("Closing client connection!");
            }
            catch (IOException)
            {
                Console.WriteLine("Problem with client communication. Exiting thread.");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }


        public static void Main()
        {
            TcpListener listener = null;
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 7412);
                listener.Start();
                Console.WriteLine("MultiThreadedEchoServer started...");
                while (true)
                {
                    Console.WriteLine("Waiting for incoming client connections...");
                    TcpClient client = listener.AcceptTcpClient();
                    Console.WriteLine("Accepted new client connection...");
                    Thread t = new Thread(ProcessClientRequests);
                    t.Start(client);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (listener != null)
                {
                    listener.Stop();
                }
            }
        }
    }
}
