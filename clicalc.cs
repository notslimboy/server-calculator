

using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

public class EchoClient
{
    public static void Main()
    {
        try
        {
            TcpClient client = new TcpClient("127.0.0.1", 7412);
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
            String s = String.Empty;
            string d = string.Empty;
            string a = string.Empty;
            while (true)
            {
                Console.Write("Enter number /n");
                s = Console.ReadLine();
                Console.WriteLine();
                writer.WriteLine(s);
                writer.Flush();

                Console.Write("Enter Operator /n");
                d = Console.ReadLine();
                Console.WriteLine();
                writer.WriteLine(d);
                writer.Flush();
               

                Console.Write("Enter number /n");
                a = Console.ReadLine();
                Console.WriteLine();
                writer.WriteLine(a);
                writer.Flush();
                String server_string = reader.ReadLine();
                Console.WriteLine(server_string);

            }
            reader.Close();
            writer.Close();
            client.Close();

        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    } // end Main()
} // end class definition